import numpy as np
import math
import matplotlib.pyplot as plt

## 1.1 Quelques fonctions auxiliaires

# Question 1 :
#fonction relerr paramétrée par I et Ibis et qui retourne l’erreur relative
def relerr(i, ibis):
    if(i!=0):
        return abs(i-ibis)/abs(i)
    else :
        return "I=0"
    #assert(i!=0)
   
# Test de la fonction 
print("QUESTION 1 :")
print("Le code fonctionne : ", relerr(1, 2))
print("On a une erreur : ", relerr(0, 2))

# Question 2 :
#fonction nbbits paramétrée par I et Ibis et qui retourne une approximation du nombre de bits exacts de la mantisse de Ibis

def nbbits(i, ibis):
    err = relerr(i, ibis)
    if (isinstance(err,(int,float))):
            return -math.log(err,2)
    else:
        return "I=0 donc impossible"
    
print("\nQUESTION 2 :")
print("Le code fonctionne : ", nbbits(1, 2))
print("On a une erreur : ", nbbits(0, 2))

# Question 3 :
# Supposons I = 1024 et Ibis = 1023. Que doivent valoir approximativement l’erreur relative et le nombre de bits exacts de la mantisse de Ibis (faire le calcul de tête). 
i=1024
ibis=1023
print("\nQUESTION 3 :")
print("Calcul de l'erreur relative de I =",i," et Ibis =",ibis,": ", nbbits(1024, 1023),"\n")

# Vos fonctions retournent-elles le résultat correct ?

# On obtient un résultat cohérent. Vérification à la main

## 1.2 Premier exemple

# Question 4 :
# Fonction f1 : 
def f1(x):
  t1 = 2 + x
  t2 = math.log(t1)
  t3 = t2 ** 2
  t7 = math.sqrt(x)
  t9 = math.cos(t7)
  t14 = math.sin(t7)
  t16 = math.exp(0.1e1 / t2 + t14 + 1)
  return (-0.1e1 / t3 / t1 + 0.1e1 / t7 * t9 / 2) * t16

x=2
print("\nQUESTION 4 :")
print("Valeur de f1 pour x=",x,":", round(f1(x),3))


# Fonction p1:
def p1(x):
    t2 = math.log(2 + x)
    t4 = math.sqrt(x)
    t5 = math.sin(t4)
    return math.exp(0.1e1 / t2 + t5 + 1)

print("Valeur de p1 pour x=",x,":", round(p1(x),3))



## 1.3 Deuxième exemple

# Question 5 :
# Fonction f2 : 
def f2(x):
    return 2/((2*(x**2))-(2*x)+1)

x=0
print("\nQUESTION 5 :")
print("Valeur de f2 pour x=",x,":", round(f2(x),3))


# Fonction p1:
def p2(x):
    return 2*np.arctan((2*x)-1)

print("Valeur de p2 pour x=",x,":", round(p2(x),3))

## 1.4 La méthode des trapèzes et celle de Simpson

# Question 6 :
#  fonction Trapezes paramétrée par une fonction f, les bornes a et b de l’intervalle d’intégration, le nombre de pas N et qui retourne l’approximation de l’intégrale de f entre a et b calculée par la méthode des trapèzes composite, en N pas.

def Trapezes(f,a,b,N):
    h= (b-a)/N
    somme = (f(a)/2) + (f(b)/2)
    for i in range (1,N):
        somme +=f(a+i*h)
    return h*somme
   
print("\nQUESTION 6 :")
print("Valeur tes pour la formule des trapèzes avec f2 entre 1 et 4 avec un pas de 10 :", round(Trapezes(f2,1,4,10),5))


# Question 7 :
#  fonction Simpson paramétrée par une fonction f, les bornes a et b de l’intervalle d’intégration, le nombre de pas N et qui retourne l’approximation de l’intégrale de f entre a et b calculée par la méthode des trapèzes composite, en N pas.

def Simpson(f,a,b,N):
    h= (b-a)/N
    somme = f(a) + f(b)
    for i in range (1,N):
        if(i%2==0):
            somme +=2*f(a+h*i)
        else:
            somme +=4*f(a+h*i)
    return (h/3)*somme


print("\nQUESTION 7 :")
print("Valeur tes pour la formule de simpson avec f2 entre 1 et 4 avec un pas de 10 :", round(Simpson(f2,1,4,10),5))
print("Valeur tes pour la formule de simpson avec f2 entre 1 et 4 avec un pas de 40 :", round(Simpson(f2,1,4,40),5))

# Si temps faire une nouvelle fonction qui fait deux boucles distinctes avec une boucle pour les paires et une pour les impaires


# Question 8 :
# En utilisant les fonctions f et p codées précédemment, vérifiez expérimentalement que vos fonctions sont correctes.
N=2000
a=1
b=4
def Integrale(a,b) :
    return p2(b)-p2(a)
tableau = np.array([2,4,8,16,32,64,128,256,512,1024])

print("\nQUESTION 8 :")
print("La formule des trapèzes :")
for i in tableau:
    print("     nbbits pour la méthode trapèze ",i,":", nbbits(Trapezes(f2,a,b,i),Integrale(a,b)))
# on a bien des pas de 2

print("La formule de Simpson :")
for i in tableau:
    print("     nbbits pour la méthode de Simpson ",i,":", nbbits(Simpson(f2,a,b,i),Integrale(a,b)))
# on niveau des données à la fin, on a bien des pas de 4
    
print("On peut aussi regarder si on obtient les mêmes valeurs pour f2 :")
print("- Théorique : ", str(p2(b)-p2(a)))
print("- Simpson : ", str(Simpson(f2,a,b,N)))
print("- trapèze : ", str(Trapezes(f2,a,b,N)))

# On obtient les mêmes valeurs donc on en déduit que les deux algorithmes sont ok...

print("On peut aussi regarder si on obtient les mêmes valeurs pour f1 :")
print("- Théorique : ", str(p1(b)-p1(a)))
print("- Simpson : ", str(Simpson(f1,a,b,N)))
print("- trapèze : ", str(Trapezes(f1,a,b,N)))

# 1.5 Étude expérimentale de l’ordre des méthodes

# Question 9 :
# Pour N = 2k avec k = 2, 3, . . . , 16, 
# calculer le nombre B de bits exacts de la mantisse de I0 Effectuer ce calcul pour chacun des deux exemples et chacune des deux méthodes. Visualiser les résultats sous la forme d’un graphique :
print("\nQUESTION 9 :")

# On définit les variables
a=1
b=4
k=16

# on crée un tableau pour stocker les résultats
x = (k+1)*[0]

# On défini les tableaux de valeurs
# Pour F1 :
theoriqueF1 = (k+1)*[0]
trapezesF1 = (k+1)*[0]
simpsonF1 = (k+1)*[0]

bits_trapezeF1 = (k+1)*[0]
bits_simpsonF1 = (k+1)*[0]

# Pour F2 :
theoriqueF2 = (k+1)*[0]
trapezesF2 = (k+1)*[0]
simpsonF2 = (k+1)*[0]

bits_trapezesF2 = (k+1)*[0]
bits_simpsonF2 = (k+1)*[0]

# On calcule pour toutes les valeurs
for i in range(2,k+1):
    x[i]=i

    #---- Pour F1 :-----
    # Valeur Théorique
    theoriqueF1[i] = p1(b)-p1(a)

    #Valeur pour les trapèzes
    trapezesF1[i] = Trapezes(f1, a, b, 2**i)
    bits_trapezeF1[i] = nbbits(theoriqueF1[i], trapezesF1[i])

    #Valeur pour Simpson
    simpsonF1[i] = Simpson(f1, a, b, 2**i)
    bits_simpsonF1[i] = nbbits(theoriqueF1[i], simpsonF1[i])
    
    #---- Pour F2 :-----
    # Valeur Théorique
    theoriqueF2[i] = p2(b)-p2(a)

    #Valeur pour les trapèzes
    trapezesF2[i] = Trapezes(f2, a, b, 2**i)
    bits_trapezesF2[i] = nbbits(theoriqueF2[i], trapezesF2[i])

    #Valeur pour Simpson
    simpsonF2[i] = Simpson(f2, a, b, 2**i)
    bits_simpsonF2[i] = nbbits(theoriqueF2[i], simpsonF2[i])


# on affiche les nuages de points du nombres de bits pour la fonction f1 pour la formules des trapèzes et la formules de simpsons :
plt.scatter(x, bits_trapezeF1, color="chocolate", label="Formule des trapèzes f1")
plt.scatter(x, bits_simpsonF1, color="coral", label="Formule de Simpson f1")

# on affiche les nuages de points du nombres de bits pour la fonction f2 pour la formules des trapèzes et la formules de simpsons :
plt.scatter(x, bits_trapezesF2, color="green", label="Formule des trapèzes f2")
plt.scatter(x, bits_simpsonF2, color="lightgreen", label="Formule de Simpson f2")

# On affiche la légende
plt.legend()
plt.title("Représentation des données suivant la méthode")
# On affiche le resultat
plt.show()

print("\n Q1 : Que représentent leur pente ?")
print("Leur pente représente l'ordre de la méthode. PLus l'ordre est élevé, plus la méthode est efficace\n")

print("\n Q2 : Comment expliquer que passée, une certaine valeur, le nombre de bits exacts n’augmente plus ?")
print("A partir d'une certaine valeur, le nombre de bits exacts n’augmente plus. Ce phénomène se produit vers les valeurs 48/50 et s'explique du fait que
le nombre de bits exacts de la mantisse de I ne peut pas être supérieur à 53.
C'est ce qui justifie que les données stagne à partir d'un certain seuil.\n")







