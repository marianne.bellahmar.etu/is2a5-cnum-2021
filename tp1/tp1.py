# I/ Interpolation Polynomiale

# 1.1 L'algorithme de Neville
#Q1 :
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d



# On entre en paramètre les coordonnées des points sous forme de tableau
Tx = np.array([1,2,3,5])
Ty = np.array([1,4,2,5])

#FAUX
def Neville2(Tx, Ty,x):
    n= len(Tx)
    result=[0]*n
    for m in range (n):
        for i in range (n):
           test = ((Tx[i]-x)*Ty[i+1]+(x-Tx[i+1])*Ty[i])/(Tx[i]-Tx[i+1])
           print(test)
           
def Neville(Tx, Ty,x):         
    n = len(Tx)
    p = n*[0]
    for j in range(n):
        for i in range(n-j):
            if j == 0:
                p[i] = Ty[i]
            else:
                p[i] = ((Tx[i]-x)*p[i+1]+(x-Tx[i+j])*p[i])/ (Tx[i]-Tx[i+j])

    print("Le resultat avec x=", x, "vaut ", p[0])
    return p[0]
    # On recupère p0 ca il s'agit du nième élément présent sur la premoère ligne d'indice 0

# On lance la méhode
Neville(Tx, Ty, 2)



#Q2 :
# On affiche le graphique des points
#plt.plot(Tx, Ty, color="blue", label="points expérimentaux")

plt.plot(Tx, Ty, color="darkturquoise", label="points expérimentaux")

x = np.linspace(1, 5, 30)
y_neville = Neville(Tx, Ty, x)

plt.plot(x, y_neville,color="saddlebrown", label="neville(x)")
plt.legend()

plt.show()

